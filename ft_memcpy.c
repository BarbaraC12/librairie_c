/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcano <bcano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/04 13:47:43 by bcano             #+#    #+#             */
/*   Updated: 2021/02/27 20:51:41 by bcano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *dst, const void *src, size_t n)
{
	size_t				i;
	unsigned char		*sd;
	unsigned char		*sr;

	sd = dst;
	sr = (void *)src;
	i = 0;
	if (!sd && !sr)
		return (NULL);
	while (i < n)
	{
		sd[i] = sr[i];
		i++;
	}
	return (dst);
}

void	*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	size_t				i;
	unsigned char		*sd;
	unsigned char		*sr;
	unsigned char		uc;

	sd = dst;
	sr = (void *)src;
	uc = c;
	i = 0;
	while (i < n)
	{
		sd[i] = sr[i];
		i++;
		if (sr[i - 1] == uc)
			return (dst + i);
	}
	return (NULL);
}
