/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_calloc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcano <bcano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/04 11:52:29 by bcano             #+#    #+#             */
/*   Updated: 2021/05/25 14:35:23 by bcano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_bzero(void *s, size_t n)
{
	size_t	i;
	char	*str;

	i = 0;
	str = s;
	if (n == 0)
		return ;
	while (i < n)
	{
		str[i] = 0;
		i++;
	}
}

static void	*ft_balloc(size_t size)
{
	void	*tamp;

	tamp = malloc(size);
	if (!tamp)
		return (NULL);
	ft_bzero(tamp, size);
	return (tamp);
}

void	*ft_calloc(size_t nmemb, size_t size)
{
	void	*result;

	result = (void *)ft_balloc(size * nmemb);
	if (!result)
		return (NULL);
	return (result);
}
