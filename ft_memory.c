/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memory.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcano <bcano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/04 13:48:16 by bcano             #+#    #+#             */
/*   Updated: 2021/02/27 20:59:56 by bcano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memset(void *b, int c, size_t len)
{
	size_t		i;
	char		*s;

	s = b;
	i = 0;
	while (i < len)
	{
		s[i] = c;
		i++;
	}
	return (b);
}

int	ft_memcmp(const void *s1, const void *s2, size_t n)
{
	size_t		i;
	char		*ss1;
	char		*ss2;

	ss1 = (void *)s1;
	ss2 = (void *)s2;
	i = 0;
	while (i < n)
	{
		if (ss1[i] != ss2[i])
			return ((unsigned char)ss1[i] - (unsigned char)ss2[i]);
		i++;
	}
	return (0);
}

void	*ft_memchr(const void *s, int c, size_t n)
{
	size_t	i;
	char	*ss;

	i = 0;
	ss = (void *)s;
	while (i < n)
	{
		if (ss[i] == c)
			return (ss + i);
		i++;
	}
	return (NULL);
}

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	size_t				i;
	unsigned char		*sd;
	const unsigned char	*sr;

	sd = (unsigned char *)dst;
	sr = (unsigned char *)src;
	i = 0;
	if (dst <= src)
		ft_memcpy(dst, src, len);
	else
	{
		sd = dst + len - 1;
		sr = src + len - 1;
		while (len--)
		{
			sd[i] = sr[i];
			i--;
		}
	}
	return (dst);
}
