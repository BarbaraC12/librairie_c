#include <unistd.h>
#include <stdio.h>
#include <stdarg.h>

int		put_base(long int nb, int base)
{
	int		i;
	char	*hexa = "0123456789abcdef";
	char	*dec = "0123456789";
	int		ret = 0;

	if (nb / base != 0)			
			ret += put_base(nb / base, base);
	i = nb % base;
	if (base == 16)
		ret += write(1, &hexa[i], 1);
	if (base == 10)
		ret += write(1, &dec[i], 1);
	return ret;
}

int             put_str(char *str)
{
        int             ret = 0;

		if (str == NULL)
			return(put_str("(null)"));
        while (str[ret])
                ret += write(1, &str[ret}, 1);
        return ret;
}

int             put_num(int num)
{
        int             ret = 0;

        if (num < 0)
        {
                ret += write(1, "-", 1);
				num *= -1;
        }
        if (num == 0)
               return (write(1, "0", 1));
		ret += put_base(num, 10);
        return ret;
}

int             put_hex(unsigned int unum)
{
        int             ret = 0;
        if (unum == 0)
                return (write(1, "0", 1));
        ret += put_base(unum, 16);
        return ret;
}

int             data_arg(char format, va_list arg)
{
        char            *str;
        int                     num;
        unsigned        unum;

        if (format == 's')
        {
                str = (char *)va_arg(arg, char *);
                return (put_str(str));
        }
        else if (format == 'd')
        {
                num = (int)va_arg(arg, int);
                return (put_num(num));
        }
        else if (format == 'x')
        {
                unum = (unsigned long int)va_arg(arg, unsigned int);
                return (put_hex(unum));
        }
        return 0;
}

int             ft_print(const char *format, ... )
{
        int     ret = 0;
        va_list         args;

        va_start(args, format);
        while (*format)
        {
                if (*format != '%')
                        ret += write (1, &*format, 1);
                else
                {
                        format++;
                        ret += data_arg(*format, args);
                }
                format++;
        }
        va_end(args);
        return ret;
}

int main(void)
{
        int age = 251431438;
        int     ret_ft = 0;
        int ret_pf = 0;

        ret_ft = ft_print("Hello work\n");
        ret_pf = printf("Hello work\n");
        printf("MY: %d | PF: %d\n\n", ret_ft, ret_pf);
        ret_ft = ft_print("je chante: %s\n", NULL);
        ret_pf = printf("je chante: %s\n", NULL);
        printf("MY: %d | PF: %d\n\n", ret_ft, ret_pf);
        ret_ft = ft_print("je chante: %s\n", "lalala lylou");
        ret_pf = printf("je chante: %s\n", "lalala lylou");
        printf("MY: %d | PF: %d\n\n", ret_ft, ret_pf);
        ret_ft = ft_print("J'ai %d ans\n", age);
        ret_pf = printf("J'ai %d ans\n", age);
        printf("MY: %d | PF: %d\n\n", ret_ft, ret_pf);
        ret_ft = ft_print("Hexage: %x\n", age*1000);
        ret_pf = printf("Hexage: %x\n", age*1000);
        printf("MY: %d | PF: %d\n", ret_ft, ret_pf);

        return 0;
}
