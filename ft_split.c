/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcano <bcano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/04 14:09:51 by bcano             #+#    #+#             */
/*   Updated: 2021/05/28 19:09:50 by bcano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void	*free_it(char **spt)
{
	int		i;

	i = 0;
	while (spt[i])
	{
		free(spt[i++]);
	}
	free(spt);
	return (NULL);
}

char	**ft_split(char const *s, char c)
{
	int		j;
	int		i;
	char	**spt;

	j = 0;
	spt = (char **)malloc(sizeof(char *) * (ft_countw(s, c) + 1));
	if (!s || !spt)
		return (NULL);
	while (*s)
	{
		while (*s == c && *s)
			s++;
		if (*s != c && *s)
		{
			i = 0;
			spt[j] = (char *)malloc(sizeof(char) * (ft_lenw(s, c) + 1));
			if (!spt[j])
				return (free_it(spt));
			while (*s && *s != c)
				spt[j][i++] = (char)*s++;
			spt[j++][i] = '\0';
		}
	}
	spt[j] = NULL;
	return (spt);
}
