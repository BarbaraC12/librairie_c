NAME		= libft.a

SRCS		= ft_atoi.c ft_itoa.c \
			ft_calloc.c ft_isalnum.c \
			ft_isascii.c ft_memory.c \
			ft_strlen.c ft_split.c \
			ft_put.c ft_put_fd.c \
			ft_strcat.c ft_substr.c \
			ft_strchr.c ft_strcmp.c \
			ft_strcpy.c ft_strdup.c \
			ft_strjoin.c ft_strmapi.c \
			ft_strstr.c ft_strtrim.c \
			ft_toup_tolow.c get_next_line.c \
			ft_lstnew.c ft_lstadd.c \
			ft_lstmap.c ft_memcpy.c

OBJS_PATH	= ./objs/
TMP			= ${SRCS:.c=.o}
OBJS		= $(addprefix ${OBJS_PATH},${TMP})

HEADER		= includes

CC			= clang
CFLAGS 		= -Wall -Wextra -Werror

$(NAME):	${OBJS}
			ar -rc ${NAME} ${OBJS}

${OBJS_PATH}%.o: %.c
			mkdir -p ${OBJS_PATH}
			${CC} -c ${CFLAGS} -o $@ $< -I ${HEADER}

all:		${NAME}

clean:
			@rm -rf ${OBJS_PATH}

fclean:		clean
			@rm -rf ${NAME}

re:			fclean all 
			@touch ~/.reset

.PHONY: 	all fclean clean re
