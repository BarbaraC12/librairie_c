/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcano <bcano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/04 14:29:26 by bcano             #+#    #+#             */
/*   Updated: 2020/05/11 22:44:01 by bcano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strtrim(char const *s1, char const *set)
{
	size_t	deb;
	size_t	cf;
	char	*str;

	deb = 0;
	if (!s1 || !set)
		return (NULL);
	while (s1[deb] && ft_strchr(set, s1[deb]))
		deb++;
	cf = ft_strlen(s1 + deb);
	if (cf)
		while (s1[cf + deb - 1] != 0
			&& ft_strchr(set, s1[cf + deb - 1]) != 0)
			cf--;
	str = malloc(sizeof(char) * cf + 1);
	if (!str)
		return (NULL);
	ft_strncpy(str, s1 + deb, cf);
	str[cf] = '\0';
	return (str);
}
