/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcano <bcano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/04 13:43:46 by bcano             #+#    #+#             */
/*   Updated: 2021/10/19 15:00:41 by bcano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	int_len(int nbr)
{
	int				count;
	unsigned int	nb;

	if (nbr == 0)
		return (1);
	count = 0;
	nb = nbr;
	if (nbr < 0)
	{
		count++;
		nb *= -1;
	}
	while (nb > 0)
	{
		nb /= 10;
		count++;
	}
	return (count);
}

static char	*number(char *str, int len, long int n)
{
	long int	unbr;
	int			is_neg;

	str[len] = '\0';
	unbr = n;
	is_neg = 0;
	if (n < 0)
	{
		is_neg = 1;
		str[0] = '-';
		unbr = -n;
	}
	len--;
	while (len >= is_neg)
	{
		str[len] = unbr % 10 + '0';
		unbr /= 10;
		len--;
	}
	return (str);
}

char	*ft_itoa(int n)
{
	char			*str;
	int				len;

	len = int_len(n);
	str = (char *)malloc(sizeof(char) * (len + 1));
	if (!str)
		return (NULL);
	str = number(str, len, n);
	return (str);
}
