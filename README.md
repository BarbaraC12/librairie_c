# My library

This is 42 school's project. 

:warning: **Warning**: It is educational project.

:warning: **Warning**: You can take inspiration from it but please don't copy / paste what you don't understand.

Grade : 115 ✅

### Functions added:
- Very simplified printf (flags s, d, x)
- Union and inter
- Get next line: int get_next_line(char **line)
- ft_ternary (ternary who are prohibited at 42)
- ft_strrev (reverse the string pass in arg)

### To submodule this lib to github project:

```sh
mkdir <project_name> ; cd <project_name>
git init
git submodule add https://github.com/BarbaraC12/libc.git
```
