/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_put.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcano <bcano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/04 13:49:26 by bcano             #+#    #+#             */
/*   Updated: 2021/02/27 20:44:29 by bcano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_putchar(char const c)
{
	return ((int)write(1, &c, 1));
}

int	ft_putstr(char const *s)
{
	return ((int)write(1, s, ft_strlen(s)));
}

int	ft_putnbr(int n)
{
	int ret;
	
	ret = 0;
	if (n == -2147483648)
		ret += ft_putstr("-2147483648");
	else if (n < 0)
	{
		ret += ft_putchar('-');
		ret += ft_putnbr(-n);
	}
	else if (n >= 10)
	{
		ret += ft_putnbr(n / 10);
		ret += ft_putchar((n % 10) + '0');
	}
	else
		ret += ft_putchar(n + '0');
	return (ret);
}

void	ft_putendl(char *s)
{
	ft_putstr(s);
	ft_putchar('\n');
}
