/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcano <bcano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/04 14:28:37 by bcano             #+#    #+#             */
/*   Updated: 2020/05/04 17:13:11 by bcano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *meule, const char *aiguille)
{
	size_t		i;
	size_t		j;

	i = 0;
	if (*aiguille == '\0')
		return ((char *)meule);
	while (meule[i])
	{
		j = 0;
		while (meule[i + j] == aiguille[j])
		{
			if (aiguille[j + 1] == '\0')
				return (&((char *)meule)[i]);
			j++;
		}
		i++;
	}
	return (NULL);
}

char	*ft_strnstr(const char *meule, const char *aiguille, size_t len)
{
	size_t		i;
	size_t		j;

	i = 0;
	if (*aiguille == '\0')
		return ((char *)meule);
	while (meule[i] && i < len)
	{
		j = 0;
		while (meule[i + j] == aiguille[j] && i + j < len)
		{
			if (aiguille[j + 1] == '\0')
				return (&((char *)meule)[i]);
			j++;
		}
		i++;
	}
	return (NULL);
}

char	*ft_strrev(char *str)
{
	char	tmp;
	int		i;
	int		j;

	i = 0;
	j = ft_strlen(str) - 1;
	while (i < j)
	{
		tmp = str[i];
		str[i] = str[j];
		str[j] = tmp;
		j--;
		i++;
	}
	return (str);
}
