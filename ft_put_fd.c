/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_put_fd.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcano <bcano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/04 13:50:50 by bcano             #+#    #+#             */
/*   Updated: 2021/02/27 20:44:24 by bcano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putchar_fd(char c, int fd)
{
	write(fd, &c, 1);
}

void	ft_putstr_fd(char *s, int fd)
{
	if (!s)
		return ;
	while (*s)
	{
		ft_putchar_fd(*s, fd);
		s++;
	}
}

void	ft_putnbr_fd(int c, int fd)
{
	if (c == -2147483648)
		ft_putstr_fd("-2147483648", fd);
	else if (c < 0)
	{
		ft_putchar_fd('-', fd);
		ft_putnbr_fd(-c, fd);
	}
	else if (c >= 10)
	{
		ft_putnbr_fd(c / 10, fd);
		ft_putchar_fd((c % 10) + '0', fd);
	}
	else
		ft_putchar_fd(c + '0', fd);
}

void	ft_putendl_fd(char *s, int fd)
{
	ft_putstr_fd(s, fd);
	ft_putchar_fd('\n', fd);
}
